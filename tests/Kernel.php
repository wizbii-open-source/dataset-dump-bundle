<?php

namespace Tests\Wizbii\DatasetDumpBundle;

use Generator;
use League\FlysystemBundle\FlysystemBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;
use Wizbii\DatasetDumpBundle\DatasetDumpBundle;

class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function registerBundles(): Generator
    {
        yield new DatasetDumpBundle();
        yield new FlysystemBundle();
        yield new FrameworkBundle();
    }

    protected function configureContainer(ContainerBuilder $container, LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/../config/services.yaml');
    }
}
